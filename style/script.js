document.addEventListener('DOMContentLoaded', function() { // Аналог $(document).ready(function(){
    document.onreadystatechange = function() {
        var state = document.readyState
        if (state == 'interactive') {} else if (state == 'complete') {
            [].forEach.call(document.querySelectorAll('img[data-src]'), function(img) {
                img.setAttribute('src', img.getAttribute('data-src'));
                img.onload = function() {
                    img.removeAttribute('data-src');
                };
            });
            Scroll();
        }
    }
});

function Scroll() {
    const anchors = document.querySelectorAll('a[href*="#"]')

    for (let i = 0; i < 3; i++) {
        anchors[i].addEventListener('click', function(e) {
            e.preventDefault()

            const blockID = anchors[i].getAttribute('href').substr(1)

            document.getElementById(blockID).scrollIntoView({
                behavior: 'smooth',
                block: 'start'
            })
        })
    }
}

/**
 * Amplitude
 */
(function(e, t) {
    var n = e.amplitude || { _q: [], _iq: {} };
    var r = t.createElement("script");
    r.type = "text/javascript";
    r.integrity = "sha384-RsEu4WZflrqYcEacpfoGSib3qaSvdYwT4D+DrWqeBuDarSzjwUQR1jO8gDiXZd0E";
    r.crossOrigin = "anonymous";
    r.async = true;
    r.src = "https://cdn.amplitude.com/libs/amplitude-6.2.0-min.gz.js";
    r.onload = function() {
        if (!e.amplitude.runQueuedFunctions) {
            console.log("[Amplitude] Error: could not load SDK")
        }
    };
    var i = t.getElementsByTagName("script")[0];
    i.parentNode.insertBefore(r, i);

    function s(e, t) {
        e.prototype[t] = function() {
            this._q.push([t].concat(Array.prototype.slice.call(arguments, 0)));
            return this
        }
    }
    var o = function() { this._q = []; return this };
    var a = ["add", "append", "clearAll", "prepend", "set", "setOnce", "unset"];
    for (var u = 0; u < a.length; u++) { s(o, a[u]) }
    n.Identify = o;
    var c = function() {
        this._q = [];
        return this
    };
    var l = ["setProductId", "setQuantity", "setPrice", "setRevenueType", "setEventProperties"];
    for (var p = 0; p < l.length; p++) { s(c, l[p]) }
    n.Revenue = c;
    var d = ["init", "logEvent", "logRevenue", "setUserId", "setUserProperties", "setOptOut", "setVersionName", "setDomain", "setDeviceId", "enableTracking", "setGlobalUserProperties", "identify", "clearUserProperties", "setGroup", "logRevenueV2", "regenerateDeviceId", "groupIdentify", "onInit", "logEventWithTimestamp", "logEventWithGroups", "setSessionId", "resetSessionId"];

    function v(e) {
        function t(t) {
            e[t] = function() {
                e._q.push([t].concat(Array.prototype.slice.call(arguments, 0)))
            }
        }
        for (var n = 0; n < d.length; n++) { t(d[n]) }
    }
    v(n);
    n.getInstance = function(e) {
        e = (!e || e.length === 0 ? "$default_instance" : e).toLowerCase();
        if (!n._iq.hasOwnProperty(e)) { n._iq[e] = { _q: [] };
            v(n._iq[e]) }
        return n._iq[e]
    };
    e.amplitude = n
})(window, document);

amplitude.getInstance().init("43a08e7488ef9d7d6c9a2adc4671e8e4");
/*
Google
*/
window.dataLayer = window.dataLayer || [];

function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());

gtag('config', 'UA-173637813-1');

/**
 * Yandex
 */

var yaParams = {};
var xhr = new XMLHttpRequest();
xhr.open('GET', 'https://ip.up66.ru/', true);
xhr.onload = function() {
    yaParams.ip = this.responseText;
}
xhr.send();

(function(d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter65993917 = new Ya.Metrika({
                id: 65993917,
                params: window.yaParams,
                clickmap: true,
                trackLinks: true,
                accurateTrackBounce: true,
                webvisor: true
            });
        } catch (e) {}
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function() { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = "https://mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");